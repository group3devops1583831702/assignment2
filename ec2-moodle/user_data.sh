#! /bin/bash -e
#title           :user_data.sh
#description     :Auto launch EC2 instance and installs 
#author		 :Group 3 
#date            :20170610
#version         :0.1
#usage		 :This has to be run with ansible playbook
#notes           :This script is made for new AWS instance with ubuntu 16.04 LTS
#bash_version    :4.3.48(1)-release
#=============================================

# Logging user data output
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
echo "BEGIN"

# Update repos and install git core utils
echo "Updating repos"
DEBIAN_FRONTEND=noninteractive sudo apt-get update
echo "Installing unzip,dialog"
DEBIAN_FRONTEND=noninteractive apt install git-core dialog -y

# Apache
echo "Installing Apache"
DEBIAN_FRONTEND=noninteractive sudo apt-get install apache2 -y

## PHP
echo "Installing PHP"
DEBIAN_FRONTEND=noninteractive sudo apt-get install graphviz php7.0-cli aspell php7.0-pspell php7.0-curl php7.0-gd php7.0-intl php7.0-mysql php7.0-xml php7.0-xmlrpc php7.0-ldap php7.0-zip php7.0-mbstring php7.0-soap libapache2-mod-php7.0 -y

## Mysql
echo "Installing MySQL"
echo "mysql-server mysql-server/root_password password 3qq4nWIgZDzco" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password 3qq4nWIgZDzco" | sudo debconf-set-selections
DEBIAN_FRONTEND=noninteractive sudo apt-get install mysql-client mysql-server -y

# Update mysql configurtaion
echo "default_storage_engine = innodb " >> /etc/mysql/mysql.conf.d/mysqld.cnf
echo "innodb_file_per_table = 1 " >> /etc/mysql/mysql.conf.d/mysqld.cnf
echo "innodb_file_format = Barracuda" >> /etc/mysql/mysql.conf.d/mysqld.cnf

sudo service mysql restart

DB="moodle"
USER="moodleuser"
PASS="J8hc69R1Jb.32"
ROOTPASS="3qq4nWIgZDzco"

mysql -uroot -p$ROOTPASS -e "CREATE DATABASE $DB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
mysql -uroot -p$ROOTPASS -e "create user '$USER'@'localhost' IDENTIFIED BY '$PASS'";
mysql -uroot -p$ROOTPASS -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO $USER@localhost IDENTIFIED BY '$PASS'";

echo "Restarting Apache"
sudo service apache2 restart

# Download moodle
echo "Download and install Moodle"
cd /usr/local/src
git clone git://git.moodle.org/moodle.git
cd moodle
git branch -a
git branch --track MOODLE_33_STABLE origin/MOODLE_33_STABLE
git checkout MOODLE_33_STABLE

mkdir /var/www/html/moodle
cp -R /usr/local/src/moodle/* /var/www/html/moodle

mkdir /var/moodledata
chown -R www-data /var/moodledata
chmod -R 777 /var/moodledata
chmod -R 0777 /var/www/html/moodle

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""
echo "Please access http://YOUR_SERVER_IP_HERE/index.php to proceed with the installation"
echo ""
echo "Moodle Details:-"
echo "Moodle Directory : /var/www/html/moodle"
echo "Moodle Data Directory : /var/moodledata" 
echo ""
echo "Mysql DB Access details:-"
echo "DB NAME : $DB"
echo "DB USER : $USER"
echo "DB PASSWORD : $PASS"
echo "DB Port : 3306"
echo ""
echo ""
echo ""
echo "Note: MySQL Root Password is 3qq4nWIgZDzco"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "IMPORTANT NOTE:"
echo "After you have ran the installer and you have moodle setup, you NEED to revert permissions so that it is no longer writable using the below command"
echo ""
echo "sudo chmod -R 0755 /var/www/html/moodle"
echo ""
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
echo "END"


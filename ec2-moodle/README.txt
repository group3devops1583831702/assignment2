###########################################################
### Ansible playbook - Launch EC2 instance with moodle  ###
###########################################################

NOTE: This script is tested on ubuntu 16.04 LTS.

# Requirements:-
Python >= 2.6
pip
boto

##### Install necessary softwares #####
# You can run these commands on your local machine to install necessary requirements

$ sudo apt-get update
$ sudo apt-get install software-properties-common
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible


$ sudo apt-get update
$ sudo apt-get install python python-setuptools python-dev build-essential python-pip 
$ sudo pip install boto

# Reference - mk

##### Run playbook from your desktop #####

clone the git lib into cd /etc/ansible folder



# Generate access_key & Secret key from aws panel and update .boto file
If you have already created keys, update AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in the .boto file 

# To create new credentials, please follow these steps
Login to AWS console >> Click on your account name >> My security credentials >> Click on + symbol of Access Keys (Access Key ID and Secret Access Key) >> Create new access key >> Copy the keys and update .boto file

# Add keys as ENV variables
export AWS_ACCESS_KEY_ID="ACCESS_KEY_ID_HERE"
export AWS_SECRET_ACCESS_KEY="YOUR_SECRET_ACCESS_KEY_HERE"

# Now run the playbook to create new instance. Wait for few mins to complete.
ansible-playbook -i hosts ec2-moodle.yml

# In the output, you will see "'public_ip': u'xxx.xxx.xxx.xxx'". 

# Login to the server using SSH
ssh -i aws_key_pair.pem xxx.xxx.xxx.xxx -l ubuntu

# You can Moodle installation details, by running this command
tail -25 /var/log/user-data.log 

Please follow the instructions 

